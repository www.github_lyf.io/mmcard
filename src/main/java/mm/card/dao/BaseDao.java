package mm.card.dao;

import java.util.List;

public interface BaseDao<T> {
	
	int insert(T entity);
	
	int insertSelective(T entity);
	
	int updateById(T entity);
	
	int updateByIdSelective(T entity);
	
	T selectById(String id);

	long getCount();
	
	List<T> queryList(int pageNum, int pageSize);
}
