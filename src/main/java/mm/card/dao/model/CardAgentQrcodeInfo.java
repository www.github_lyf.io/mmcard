package mm.card.dao.model;

import mm.card.util.AesUtil;

public class CardAgentQrcodeInfo {

	public CardAgentQrcodeInfo() {
	}

	public CardAgentQrcodeInfo(CardTable3 tb) throws Exception {
		this.id = tb.getId();
		this.agentId = AesUtil.decrypt(tb.getAttribute1());
		this.qrcodeStart = AesUtil.decrypt(tb.getAttribute2());
		this.qrcodeEnd = AesUtil.decrypt(tb.getAttribute3());
		this.qrcodeCount = Integer.valueOf(AesUtil.decrypt(tb.getAttribute4()));
		this.qrcodeActiveCount = Integer.valueOf(AesUtil.decrypt(tb.getAttribute5()));
		this.qrcodeZipPath = AesUtil.decrypt(tb.getAttribute6());
		this.provice = AesUtil.decrypt(tb.getAttribute7());
		this.city = AesUtil.decrypt(tb.getAttribute8());
		this.county = AesUtil.decrypt(tb.getAttribute9());
		this.isValid = Integer.valueOf(AesUtil.decrypt(tb.getAttribute10()));
		this.isDelete = Integer.valueOf(AesUtil.decrypt(tb.getAttribute11()));
	}

	public CardTable3 toCardTable3() throws Exception {
		CardTable3 tb = new CardTable3();
		tb.setId(this.id);
		tb.setAttribute1(AesUtil.encrypt(this.agentId));
		tb.setAttribute2(AesUtil.encrypt(this.qrcodeStart));
		tb.setAttribute3(AesUtil.encrypt(this.qrcodeEnd));
		tb.setAttribute4(AesUtil.encrypt(this.qrcodeCount.toString()));
		tb.setAttribute5(AesUtil.encrypt(this.qrcodeActiveCount.toString()));
		tb.setAttribute6(AesUtil.encrypt(this.qrcodeZipPath));
		tb.setAttribute7(AesUtil.encrypt(this.provice));
		tb.setAttribute8(AesUtil.encrypt(this.city));
		tb.setAttribute9(AesUtil.encrypt(this.county));
		tb.setAttribute10(AesUtil.encrypt(this.isValid.toString()));
		tb.setAttribute11(AesUtil.encrypt(this.isDelete.toString()));
		return tb;
	}

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column card_agent_qrcode_info.id
	 *
	 * @mbggenerated
	 */
	private Long id;

	/**
	 * 代理商ID
	 *
	 * @mbggenerated
	 */
	private String agentId;

	/**
	 * 二维码起始ID
	 *
	 * @mbggenerated
	 */
	private String qrcodeStart;

	/**
	 * 二维码结束ID
	 *
	 * @mbggenerated
	 */
	private String qrcodeEnd;

	/**
	 * 二维码数量
	 *
	 * @mbggenerated
	 */
	private Integer qrcodeCount;

	/**
	 * 二维码激活数量
	 *
	 * @mbggenerated
	 */
	private Integer qrcodeActiveCount;

	/**
	 * 二维码压缩包磁盘路径
	 *
	 * @mbggenerated
	 */
	private String qrcodeZipPath;

	/**
	 * 省份
	 *
	 * @mbggenerated
	 */
	private String provice;

	/**
	 * 城市
	 *
	 * @mbggenerated
	 */
	private String city;

	/**
	 * 县区
	 *
	 * @mbggenerated
	 */
	private String county;

	/**
	 * 是否有效
	 *
	 * @mbggenerated
	 */
	private Integer isValid;

	/**
	 * 是否删除
	 *
	 * @mbggenerated
	 */
	private Integer isDelete;

	/**
	 * 获取
	 *
	 * @return id
	 *
	 * @mbggenerated
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置
	 *
	 * @param id
	 *
	 * @mbggenerated
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取代理商ID
	 *
	 * @return agentId 代理商ID
	 *
	 * @mbggenerated
	 */
	public String getAgentId() {
		return agentId;
	}

	/**
	 * 设置代理商ID
	 *
	 * @param agentId
	 *            代理商ID
	 *
	 * @mbggenerated
	 */
	public void setAgentId(String agentId) {
		this.agentId = agentId == null ? null : agentId.trim();
	}

	/**
	 * 获取二维码起始ID
	 *
	 * @return qrcodeStart 二维码起始ID
	 *
	 * @mbggenerated
	 */
	public String getQrcodeStart() {
		return qrcodeStart;
	}

	/**
	 * 设置二维码起始ID
	 *
	 * @param qrcodeStart
	 *            二维码起始ID
	 *
	 * @mbggenerated
	 */
	public void setQrcodeStart(String qrcodeStart) {
		this.qrcodeStart = qrcodeStart == null ? null : qrcodeStart.trim();
	}

	/**
	 * 获取二维码结束ID
	 *
	 * @return qrcodeEnd 二维码结束ID
	 *
	 * @mbggenerated
	 */
	public String getQrcodeEnd() {
		return qrcodeEnd;
	}

	/**
	 * 设置二维码结束ID
	 *
	 * @param qrcodeEnd
	 *            二维码结束ID
	 *
	 * @mbggenerated
	 */
	public void setQrcodeEnd(String qrcodeEnd) {
		this.qrcodeEnd = qrcodeEnd == null ? null : qrcodeEnd.trim();
	}

	/**
	 * 获取二维码数量
	 *
	 * @return qrcodeCount 二维码数量
	 *
	 * @mbggenerated
	 */
	public Integer getQrcodeCount() {
		return qrcodeCount;
	}

	/**
	 * 设置二维码数量
	 *
	 * @param qrcodeCount
	 *            二维码数量
	 *
	 * @mbggenerated
	 */
	public void setQrcodeCount(Integer qrcodeCount) {
		this.qrcodeCount = qrcodeCount;
	}

	/**
	 * 获取二维码激活数量
	 *
	 * @return qrcodeActiveCount 二维码激活数量
	 *
	 * @mbggenerated
	 */
	public Integer getQrcodeActiveCount() {
		return qrcodeActiveCount;
	}

	/**
	 * 设置二维码激活数量
	 *
	 * @param qrcodeActiveCount
	 *            二维码激活数量
	 *
	 * @mbggenerated
	 */
	public void setQrcodeActiveCount(Integer qrcodeActiveCount) {
		this.qrcodeActiveCount = qrcodeActiveCount;
	}

	/**
	 * 获取二维码压缩包磁盘路径
	 *
	 * @return qrcodeZipPath 二维码压缩包磁盘路径
	 *
	 * @mbggenerated
	 */
	public String getQrcodeZipPath() {
		return qrcodeZipPath;
	}

	/**
	 * 设置二维码压缩包磁盘路径
	 *
	 * @param qrcodeZipPath
	 *            二维码压缩包磁盘路径
	 *
	 * @mbggenerated
	 */
	public void setQrcodeZipPath(String qrcodeZipPath) {
		this.qrcodeZipPath = qrcodeZipPath == null ? null : qrcodeZipPath.trim();
	}

	/**
	 * 获取省份
	 *
	 * @return provice 省份
	 *
	 * @mbggenerated
	 */
	public String getProvice() {
		return provice;
	}

	/**
	 * 设置省份
	 *
	 * @param provice
	 *            省份
	 *
	 * @mbggenerated
	 */
	public void setProvice(String provice) {
		this.provice = provice == null ? null : provice.trim();
	}

	/**
	 * 获取城市
	 *
	 * @return city 城市
	 *
	 * @mbggenerated
	 */
	public String getCity() {
		return city;
	}

	/**
	 * 设置城市
	 *
	 * @param city
	 *            城市
	 *
	 * @mbggenerated
	 */
	public void setCity(String city) {
		this.city = city == null ? null : city.trim();
	}

	/**
	 * 获取县区
	 *
	 * @return county 县区
	 *
	 * @mbggenerated
	 */
	public String getCounty() {
		return county;
	}

	/**
	 * 设置县区
	 *
	 * @param county
	 *            县区
	 *
	 * @mbggenerated
	 */
	public void setCounty(String county) {
		this.county = county == null ? null : county.trim();
	}

	/**
	 * 获取是否有效
	 *
	 * @return isValid 是否有效
	 *
	 * @mbggenerated
	 */
	public Integer getIsValid() {
		return isValid;
	}

	/**
	 * 设置是否有效
	 *
	 * @param isValid
	 *            是否有效
	 *
	 * @mbggenerated
	 */
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}

	/**
	 * 获取是否删除
	 *
	 * @return isDelete 是否删除
	 *
	 * @mbggenerated
	 */
	public Integer getIsDelete() {
		return isDelete;
	}

	/**
	 * 设置是否删除
	 *
	 * @param isDelete
	 *            是否删除
	 *
	 * @mbggenerated
	 */
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
}