package mm.card.dao.model;

import java.util.Date;

import mm.card.util.AesUtil;
import mm.card.util.DateUtil;

public class CardAdminInfo {

	public CardAdminInfo() {
	}

	public CardAdminInfo(CardTable1 tb) throws Exception {
		this.id = tb.getId();
		this.adminId = AesUtil.decrypt(tb.getAttribute1());
		this.userName = AesUtil.decrypt(tb.getAttribute2());
		this.password = AesUtil.decrypt(tb.getAttribute3());
		this.userRole = AesUtil.decrypt(tb.getAttribute4());
		this.pwdErrorCount = Integer.valueOf(AesUtil.decrypt(tb.getAttribute5()));
		this.isLock = Integer.valueOf(AesUtil.decrypt(tb.getAttribute6()));
		this.createTime = DateUtil.str2DateTime(AesUtil.decrypt(tb.getAttribute7()));
		this.lastLoginTime = DateUtil.str2DateTime(AesUtil.decrypt(tb.getAttribute8()));
		this.isActive = Integer.valueOf(AesUtil.decrypt(tb.getAttribute9()));
		this.isValid = Integer.valueOf(AesUtil.decrypt(tb.getAttribute10()));
		this.isDelete = Integer.valueOf(AesUtil.decrypt(tb.getAttribute11()));
	}

	public CardTable1 toCardTable1() throws Exception {
		CardTable1 tb = new CardTable1();
		tb.setId(this.id);
		tb.setAttribute1(AesUtil.encrypt(this.adminId));
		tb.setAttribute2(AesUtil.encrypt(this.userName));
		tb.setAttribute3(AesUtil.encrypt(this.password));
		tb.setAttribute4(AesUtil.encrypt(this.userRole));
		tb.setAttribute5(AesUtil.encrypt(this.pwdErrorCount.toString()));
		tb.setAttribute6(AesUtil.encrypt(this.isLock.toString()));
		tb.setAttribute7(AesUtil.encrypt(DateUtil.dateTime2Str(this.createTime)));
		tb.setAttribute8(AesUtil.encrypt(DateUtil.dateTime2Str(this.lastLoginTime)));
		tb.setAttribute9(AesUtil.encrypt(this.isActive.toString()));
		tb.setAttribute10(AesUtil.encrypt(this.isValid.toString()));
		tb.setAttribute11(AesUtil.encrypt(this.isDelete.toString()));
		return tb;
	}

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to
	 * the database column card_admin_info.id
	 *
	 * @mbggenerated
	 */
	private Long id;

	/**
	 * 管理员ID
	 *
	 * @mbggenerated
	 */
	private String adminId;

	/**
	 * 用户名
	 *
	 * @mbggenerated
	 */
	private String userName;

	/**
	 * 密码
	 *
	 * @mbggenerated
	 */
	private String password;

	/**
	 * 角色
	 *
	 * @mbggenerated
	 */
	private String userRole;

	/**
	 * 密码错误次数
	 *
	 * @mbggenerated
	 */
	private Integer pwdErrorCount;

	/**
	 * 是否锁定
	 *
	 * @mbggenerated
	 */
	private Integer isLock;

	/**
	 * 创建时间
	 *
	 * @mbggenerated
	 */
	private Date createTime;

	/**
	 * 最后登录时间
	 *
	 * @mbggenerated
	 */
	private Date lastLoginTime;

	/**
	 * 是否激活
	 *
	 * @mbggenerated
	 */
	private Integer isActive;

	/**
	 * 是否有效
	 *
	 * @mbggenerated
	 */
	private Integer isValid;

	/**
	 * 是否删除
	 *
	 * @mbggenerated
	 */
	private Integer isDelete;

	/**
	 * 获取
	 *
	 * @return id
	 *
	 * @mbggenerated
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 设置
	 *
	 * @param id
	 *
	 * @mbggenerated
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 获取管理员ID
	 *
	 * @return adminId 管理员ID
	 *
	 * @mbggenerated
	 */
	public String getAdminId() {
		return adminId;
	}

	/**
	 * 设置管理员ID
	 *
	 * @param adminId
	 *            管理员ID
	 *
	 * @mbggenerated
	 */
	public void setAdminId(String adminId) {
		this.adminId = adminId == null ? null : adminId.trim();
	}

	/**
	 * 获取用户名
	 *
	 * @return userName 用户名
	 *
	 * @mbggenerated
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 设置用户名
	 *
	 * @param userName
	 *            用户名
	 *
	 * @mbggenerated
	 */
	public void setUserName(String userName) {
		this.userName = userName == null ? null : userName.trim();
	}

	/**
	 * 获取密码
	 *
	 * @return password 密码
	 *
	 * @mbggenerated
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * 设置密码
	 *
	 * @param password
	 *            密码
	 *
	 * @mbggenerated
	 */
	public void setPassword(String password) {
		this.password = password == null ? null : password.trim();
	}

	/**
	 * 获取角色
	 *
	 * @return userRole 角色
	 *
	 * @mbggenerated
	 */
	public String getUserRole() {
		return userRole;
	}

	/**
	 * 设置角色
	 *
	 * @param userRole
	 *            角色
	 *
	 * @mbggenerated
	 */
	public void setUserRole(String userRole) {
		this.userRole = userRole == null ? null : userRole.trim();
	}

	/**
	 * 获取密码错误次数
	 *
	 * @return pwdErrorCount 密码错误次数
	 *
	 * @mbggenerated
	 */
	public Integer getPwdErrorCount() {
		return pwdErrorCount;
	}

	/**
	 * 设置密码错误次数
	 *
	 * @param pwdErrorCount
	 *            密码错误次数
	 *
	 * @mbggenerated
	 */
	public void setPwdErrorCount(Integer pwdErrorCount) {
		this.pwdErrorCount = pwdErrorCount;
	}

	/**
	 * 获取是否锁定
	 *
	 * @return isLock 是否锁定
	 *
	 * @mbggenerated
	 */
	public Integer getIsLock() {
		return isLock;
	}

	/**
	 * 设置是否锁定
	 *
	 * @param isLock
	 *            是否锁定
	 *
	 * @mbggenerated
	 */
	public void setIsLock(Integer isLock) {
		this.isLock = isLock;
	}

	/**
	 * 获取创建时间
	 *
	 * @return createTime 创建时间
	 *
	 * @mbggenerated
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * 设置创建时间
	 *
	 * @param createTime
	 *            创建时间
	 *
	 * @mbggenerated
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * 获取最后登录时间
	 *
	 * @return lastLoginTime 最后登录时间
	 *
	 * @mbggenerated
	 */
	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	/**
	 * 设置最后登录时间
	 *
	 * @param lastLoginTime
	 *            最后登录时间
	 *
	 * @mbggenerated
	 */
	public void setLastLoginTime(Date lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	/**
	 * 获取是否激活
	 *
	 * @return isActive 是否激活
	 *
	 * @mbggenerated
	 */
	public Integer getIsActive() {
		return isActive;
	}

	/**
	 * 设置是否激活
	 *
	 * @param isActive
	 *            是否激活
	 *
	 * @mbggenerated
	 */
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}

	/**
	 * 获取是否有效
	 *
	 * @return isValid 是否有效
	 *
	 * @mbggenerated
	 */
	public Integer getIsValid() {
		return isValid;
	}

	/**
	 * 设置是否有效
	 *
	 * @param isValid
	 *            是否有效
	 *
	 * @mbggenerated
	 */
	public void setIsValid(Integer isValid) {
		this.isValid = isValid;
	}

	/**
	 * 获取是否删除
	 *
	 * @return isDelete 是否删除
	 *
	 * @mbggenerated
	 */
	public Integer getIsDelete() {
		return isDelete;
	}

	/**
	 * 设置是否删除
	 *
	 * @param isDelete
	 *            是否删除
	 *
	 * @mbggenerated
	 */
	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}
}