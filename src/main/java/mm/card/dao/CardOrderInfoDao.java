package mm.card.dao;

import mm.card.dao.model.CardOrderInfo;

public interface CardOrderInfoDao extends BaseDao<CardOrderInfo> {

}
