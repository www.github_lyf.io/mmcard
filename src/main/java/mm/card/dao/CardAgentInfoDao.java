package mm.card.dao;

import mm.card.dao.model.CardAgentInfo;

public interface CardAgentInfoDao extends BaseDao<CardAgentInfo> {

}
