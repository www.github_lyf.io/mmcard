package mm.card.dao;

import mm.card.dao.model.CardOrderSuccessInfo;

public interface CardOrderSuccessInfoDao extends BaseDao<CardOrderSuccessInfo> {

}
