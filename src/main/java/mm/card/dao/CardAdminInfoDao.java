package mm.card.dao;

import mm.card.dao.model.CardAdminInfo;

public interface CardAdminInfoDao extends BaseDao<CardAdminInfo> {

}
