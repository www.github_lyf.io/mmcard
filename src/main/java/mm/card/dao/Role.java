package mm.card.dao;

import mm.card.pojo.SystemUser;
import mm.card.pojo.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * Created by Yif on 2016/10/17.
 */
@Mapper
public interface Role {

    @Select("select * from user_role where user_id = #{id}")
    @ResultType(UserRole.class)
    List<UserRole> getRoleByUser(SystemUser user);
}
