package mm.card.dao;

import mm.card.dao.model.CardQrcodeInfo;

public interface CardQrcodeInfoDao extends BaseDao<CardQrcodeInfo> {

}
