package mm.card.dao;

import mm.card.dao.model.CardShopInfo;

public interface CardShopInfoDao extends BaseDao<CardShopInfo> {

}
