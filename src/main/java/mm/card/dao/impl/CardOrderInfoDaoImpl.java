package mm.card.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mm.card.dao.CardOrderInfoDao;
import mm.card.dao.mapper.CardTable4Mapper;
import mm.card.dao.model.CardOrderInfo;
import mm.card.dao.model.CardTable4;

@Repository
public class CardOrderInfoDaoImpl implements CardOrderInfoDao {
	
	@Autowired
	private CardTable4Mapper mapper;

	@Override
	public int insert(CardOrderInfo entity) {
		try {
			CardTable4 tb = entity.toCardTable4();
			return mapper.insert(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int insertSelective(CardOrderInfo entity) {
		try {
			CardTable4 tb = entity.toCardTable4();
			return mapper.insertSelective(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int updateById(CardOrderInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByIdSelective(CardOrderInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CardOrderInfo selectById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CardOrderInfo> queryList(int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
