package mm.card.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mm.card.dao.CardAgentQrcodeInfoDao;
import mm.card.dao.mapper.CardTable3Mapper;
import mm.card.dao.model.CardAgentQrcodeInfo;
import mm.card.dao.model.CardTable3;

@Repository
public class CardAgentQrcodeInfoDaoImpl implements CardAgentQrcodeInfoDao {
	
	@Autowired
	private CardTable3Mapper mapper;

	@Override
	public int insert(CardAgentQrcodeInfo entity) {
		try {
			CardTable3 tb = entity.toCardTable3();
			return mapper.insert(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int insertSelective(CardAgentQrcodeInfo entity) {
		try {
			CardTable3 tb = entity.toCardTable3();
			return mapper.insertSelective(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int updateById(CardAgentQrcodeInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByIdSelective(CardAgentQrcodeInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CardAgentQrcodeInfo selectById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CardAgentQrcodeInfo> queryList(int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
