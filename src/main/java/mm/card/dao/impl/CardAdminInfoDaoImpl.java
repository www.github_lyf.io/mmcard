package mm.card.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mm.card.dao.CardAdminInfoDao;
import mm.card.dao.mapper.CardTable1Mapper;
import mm.card.dao.model.CardAdminInfo;
import mm.card.dao.model.CardTable1;

@Repository
public class CardAdminInfoDaoImpl implements CardAdminInfoDao {
	
	@Autowired
	private CardTable1Mapper mapper;

	@Override
	public int insert(CardAdminInfo entity) {
		try {
			CardTable1 tb = entity.toCardTable1();
			return mapper.insert(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int insertSelective(CardAdminInfo entity) {
		try {
			CardTable1 tb = entity.toCardTable1();
			return mapper.insertSelective(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int updateById(CardAdminInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByIdSelective(CardAdminInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CardAdminInfo selectById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CardAdminInfo> queryList(int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
