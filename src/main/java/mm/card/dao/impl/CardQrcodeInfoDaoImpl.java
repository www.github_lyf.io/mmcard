package mm.card.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mm.card.dao.CardQrcodeInfoDao;
import mm.card.dao.mapper.CardTable6Mapper;
import mm.card.dao.model.CardQrcodeInfo;
import mm.card.dao.model.CardTable6;

@Repository
public class CardQrcodeInfoDaoImpl implements CardQrcodeInfoDao {
	
	@Autowired
	private CardTable6Mapper mapper;

	@Override
	public int insert(CardQrcodeInfo entity) {
		try {
			CardTable6 tb = entity.toCardTable6();
			return mapper.insert(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int insertSelective(CardQrcodeInfo entity) {
		try {
			CardTable6 tb = entity.toCardTable6();
			return mapper.insertSelective(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int updateById(CardQrcodeInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByIdSelective(CardQrcodeInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CardQrcodeInfo selectById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CardQrcodeInfo> queryList(int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
