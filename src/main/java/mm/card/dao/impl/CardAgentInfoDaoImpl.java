package mm.card.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mm.card.dao.CardAgentInfoDao;
import mm.card.dao.mapper.CardTable2Mapper;
import mm.card.dao.model.CardAgentInfo;
import mm.card.dao.model.CardTable2;

@Repository
public class CardAgentInfoDaoImpl implements CardAgentInfoDao {
	
	@Autowired
	private CardTable2Mapper mapper;

	@Override
	public int insert(CardAgentInfo entity) {
		try {
			CardTable2 tb = entity.toCardTable2();
			return mapper.insert(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int insertSelective(CardAgentInfo entity) {
		try {
			CardTable2 tb = entity.toCardTable2();
			return mapper.insertSelective(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int updateById(CardAgentInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByIdSelective(CardAgentInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CardAgentInfo selectById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CardAgentInfo> queryList(int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
