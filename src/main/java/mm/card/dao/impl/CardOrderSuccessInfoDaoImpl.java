package mm.card.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mm.card.dao.CardOrderSuccessInfoDao;
import mm.card.dao.mapper.CardTable5Mapper;
import mm.card.dao.model.CardOrderSuccessInfo;
import mm.card.dao.model.CardTable5;

@Repository
public class CardOrderSuccessInfoDaoImpl implements CardOrderSuccessInfoDao {
	
	@Autowired
	private CardTable5Mapper mapper;


	@Override
	public int insert(CardOrderSuccessInfo entity) {
		try {
			CardTable5 tb = entity.toCardTable5();
			return mapper.insert(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int insertSelective(CardOrderSuccessInfo entity) {
		try {
			CardTable5 tb = entity.toCardTable5();
			return mapper.insertSelective(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int updateById(CardOrderSuccessInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByIdSelective(CardOrderSuccessInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CardOrderSuccessInfo selectById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CardOrderSuccessInfo> queryList(int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
