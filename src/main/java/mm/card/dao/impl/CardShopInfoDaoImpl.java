package mm.card.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mm.card.dao.CardShopInfoDao;
import mm.card.dao.mapper.CardTable7Mapper;
import mm.card.dao.model.CardShopInfo;
import mm.card.dao.model.CardTable7;

@Repository
public class CardShopInfoDaoImpl implements CardShopInfoDao {
	
	@Autowired
	private CardTable7Mapper mapper;

	@Override
	public int insert(CardShopInfo entity) {
		try {
			CardTable7 tb = entity.toCardTable7();
			return mapper.insert(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int insertSelective(CardShopInfo entity) {
		try {
			CardTable7 tb = entity.toCardTable7();
			return mapper.insertSelective(tb);
		} catch (Exception e) {
			
		}
		return 0;
	}

	@Override
	public int updateById(CardShopInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByIdSelective(CardShopInfo entity) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public CardShopInfo selectById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<CardShopInfo> queryList(int pageNum, int pageSize) {
		// TODO Auto-generated method stub
		return null;
	}

}
