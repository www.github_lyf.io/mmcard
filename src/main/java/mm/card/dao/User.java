package mm.card.dao;

import mm.card.pojo.SystemUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.ResultType;
import org.apache.ibatis.annotations.Select;

/**
 * Created by Yif on 2016/10/17.
 */
@Mapper
public interface User {

    @Select("select * from user where username=#{userName}")
    @ResultType(SystemUser.class)
    SystemUser findByName(String userName);
}
