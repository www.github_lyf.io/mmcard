package mm.card.dao;

import mm.card.dao.model.CardAgentQrcodeInfo;

public interface CardAgentQrcodeInfoDao extends BaseDao<CardAgentQrcodeInfo> {

}
