package mm.card.service;


import mm.card.pojo.SystemUser;
import mm.card.pojo.UserRole;

import java.util.List;

/**
 * Created by Yif on 2016/10/17.
 */
public interface UserRoleService {

    List<UserRole> getRoleByUser(SystemUser user);
}
