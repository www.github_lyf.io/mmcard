package mm.card.service;


import mm.card.pojo.SystemUser;

/**
 * Created by Yif on 2016/10/17.
 */
public interface SystemUserService {

    SystemUser findByName(String userName);
}
