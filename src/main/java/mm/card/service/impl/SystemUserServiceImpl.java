package mm.card.service.impl;

import mm.card.dao.User;
import mm.card.pojo.SystemUser;
import mm.card.service.SystemUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Yif on 2016/10/17.
 */
@Service
public class SystemUserServiceImpl implements SystemUserService {

    @Autowired
    private User user;

    @Override
    public SystemUser findByName(String userName) {
        return user.findByName(userName);
    }
}
