package mm.card.service.impl;


import mm.card.dao.Role;
import mm.card.pojo.SystemUser;
import mm.card.pojo.UserRole;
import mm.card.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Created by Yif on 2016/10/17.
 */

@Service
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private Role role;

    @Override
    public List<UserRole> getRoleByUser(SystemUser user) {
            return role.getRoleByUser(user);
    }
}
