package mm.card.pojo;

import java.util.List;

/**
 * Created by Yif on 2016/10/18.
 */
public class Response extends SystemUser{

    private List userRole;

    private String sessionId;

    public List getUserRole() {
        return userRole;
    }

    public void setUserRole(List userRole) {
        this.userRole = userRole;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
