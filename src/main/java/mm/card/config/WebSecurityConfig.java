package mm.card.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by Yif on 2016/10/17.
 */

//http://kielczewski.eu/2014/12/spring-boot-security-application/
 
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


    @Autowired
    private MyAuthenticationProvider provider;//自定义验证

    @Autowired
    private UserDetailsService userDetailsService;//自定义用户服务

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception{
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.authorizeRequests().antMatchers("/", "/user/hello").permitAll()./*antMatchers("/user/thy").access("hasRole('ROLE_ADMIN')").*/
                anyRequest().authenticated().and().
                formLogin()./*loginProcessingUrl("/user/login").*/loginPage("/user/login").defaultSuccessUrl("/user/success").permitAll().and().csrf()/*.and().logout().logoutUrl("/user/logout")*//*.logoutSuccessUrl("/user/login?logout")*//*.invalidateHttpSession(true).deleteCookies("JSESSIONID").permitAll()*/;
               /* formLogin().loginPage("/user/login").loginProcessingUrl("/user/login")*//*登录页面下表单提交的路径*//*.failureUrl("/user/login")*//*登录失败后跳转的路径*//*.
                defaultSuccessUrl("/user/success").and().csrf()*//*启用防跨站伪请求攻击,默认开启*//*.and().logout().*//*logoutUrl("/user/logout").permitAll().logoutSuccessUrl("/user/login?logout=true").*//*permitAll();*/
        httpSecurity.logout().logoutUrl("/user/logout").invalidateHttpSession(true).permitAll();

    }

    //自定义内存 用户名 密码 显示不符合开发要求 测试可以使用
/*    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        System.out.println("登录login进入");
        auth.inMemoryAuthentication().withUser("user").password("password").roles("USER");
    }*/

    //自定义验证工具
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth){
        auth.authenticationProvider(provider);
    }

	/* @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    } */
	
	
    //开启@PreAuthorize注解拦截
    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

/*    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }*/

}
