package mm.card.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class BigDecimalUtil {

	private static DecimalFormat millionthDf = new DecimalFormat("0.000000");
	
	private static DecimalFormat percentileDf = new DecimalFormat("0.00");
	
	public static String millionthFormat(BigDecimal value){
		return millionthDf.format(value);
	}
	
	public static String percentileFormat(BigDecimal value){
		return percentileDf.format(value);
	}
}
