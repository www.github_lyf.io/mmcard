package mm.card.util;

import xin.eplus.qrcode.*;
import xin.eplus.qrcode.client.j2se.BufferedImageLuminanceSource;
import xin.eplus.qrcode.common.BitMatrix;
import xin.eplus.qrcode.common.HybridBinarizer;
import xin.eplus.qrcode.qrcode.decoder.ErrorCorrectionLevel;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.Random;

/**
 * 二维码生成
 * Created by Yif on 2016/10/19.
 */
public class QRCodeUtil {

    private final static String CHARSET = "UTF-8";

    private final static String FORMAT_NAME = "jpg";

    //二维码尺寸 430*430
    private final static int QR_CODE_SIZE_BIG = 430;

    //258*258
    private final static int QR_CODE_SIZE_MID = 258;

    //128*128
    private final static int QR_CODE_SIZE_SMAL = 128;

    //LOGO高度
    private final static int WIDTH = 60;

    //LOGO宽度
    private final static int HEIGHT = 60;

    //创建图片
    private static BufferedImage createImage(String content,String imgPath,boolean needCompress,int size) throws WriterException, IOException {
        Hashtable hints = new Hashtable();
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
        hints.put(EncodeHintType.CHARACTER_SET, CHARSET);
        hints.put(EncodeHintType.MARGIN, 1);
        BitMatrix bitMatrix = null;
        if(size == 0){
             bitMatrix = new MultiFormatWriter().encode(content,
                    BarcodeFormat.QR_CODE, QR_CODE_SIZE_SMAL, QR_CODE_SIZE_SMAL, hints);
        }else if(size == 1){
             bitMatrix = new MultiFormatWriter().encode(content,
                    BarcodeFormat.QR_CODE, QR_CODE_SIZE_MID, QR_CODE_SIZE_MID, hints);
        }else{
             bitMatrix = new MultiFormatWriter().encode(content,
                    BarcodeFormat.QR_CODE, QR_CODE_SIZE_BIG, QR_CODE_SIZE_BIG, hints);
        }
        int width = bitMatrix.getWidth();
        int height = bitMatrix.getHeight();
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, bitMatrix.get(x, y) ? 0xFF000000
                        : 0xFFFFFFFF);
            }
        }
        if (imgPath == null || "".equals(imgPath)) {
            return image;
        }
        // 插入图片
        QRCodeUtil.insertImage(image, imgPath, needCompress);
        return image;
    }

    private static void  insertImage(BufferedImage source, String imgPath,
                                     boolean needCompress) throws IOException {
        File file = new File(imgPath);
        if (!file.exists()) {
            System.err.println(""+imgPath+"   该文件不存在！");
            return;
        }
        Image src = ImageIO.read(new File(imgPath));
        int width = src.getWidth(null);
        int height = src.getHeight(null);
        if(needCompress){// 压缩LOGO
            if (width > WIDTH) {
                width = WIDTH;
            }
            if (height > HEIGHT) {
                height = HEIGHT;
            }
            Image image = src.getScaledInstance(width, height,
                    Image.SCALE_SMOOTH);
            BufferedImage tag = new BufferedImage(width, height,
                    BufferedImage.TYPE_INT_RGB);
            Graphics g = tag.getGraphics();
            g.drawImage(image, 0, 0, null); // 绘制缩小后的图
            g.dispose();
            src = image;
        }
        // 插入LOGO
        Graphics2D graph = source.createGraphics();
        int x = (QR_CODE_SIZE_BIG - width) / 2;
        int y = (QR_CODE_SIZE_BIG - height) / 2;
        graph.drawImage(src, x, y, width, height, null);
        Shape shape = new RoundRectangle2D.Float(x, y, width, width, 6, 6);
        graph.setStroke(new BasicStroke(3f));
        graph.draw(shape);
        graph.dispose();
    }

    public static void encode(String content, String imgPath, String destPath,
                              boolean needCompress,int size) throws Exception {
        BufferedImage image = null;
        if(size == 0){
            image = QRCodeUtil.createImage(content, imgPath,
                    needCompress,size);
        }else if (size == 1){
            image = QRCodeUtil.createImage(content, imgPath,
                    needCompress,size);
        }else{
            image = QRCodeUtil.createImage(content, imgPath,
                    needCompress,size);
        }
        mkdirs(destPath);
//        mkdirs("d:/MyWorkDoc/big");
//        mkdirs("d:/MyWorkDoc/mid");
        String file = new Random().nextInt(999999999)+".jpg";//图片命名
        ImageIO.write(image, FORMAT_NAME, new File(destPath+"/"+file));
//        ImageIO.write(image2, FORMAT_NAME, new File("d:/MyWorkDoc/big"+"/"+file));
//        ImageIO.write(image3, FORMAT_NAME, new File("d:/MyWorkDoc/mid"+"/"+file));
    }

    public static void mkdirs(String destPath) {
        File file =new File(destPath);
        //当文件夹不存在时，mkdirs会自动创建多层目录，区别于mkdir．(mkdir如果父目录不存在则会抛出异常)
        if (!file.exists() && !file.isDirectory()) {
            file.mkdirs();
        }
    }

/*    public static void encode(String content, String imgPath, String destPath)
            throws Exception {
        QRCodeUtil.encode(content, imgPath, destPath, false);
    }*/

    public static void encode(String content, String destPath,
                              boolean needCompress,int size) throws Exception {
        QRCodeUtil.encode(content, null, destPath, needCompress,size);
    }


 /*   public static void encode(String content, String destPath) throws Exception {
        QRCodeUtil.encode(content, null, destPath, false);
    }*/


    public static void encode(String content, String imgPath,
                              OutputStream output, boolean needCompress) throws Exception {
        BufferedImage image = QRCodeUtil.createImage(content, imgPath,
                needCompress,1);
        ImageIO.write(image, FORMAT_NAME, output);
    }


    public static void encode(String content, OutputStream output)
            throws Exception {
        QRCodeUtil.encode(content, null, output, false);
    }


    public static String decode(File file) throws Exception {
        BufferedImage image;
        image = ImageIO.read(file);
        if (image == null) {
            return null;
        }
        BufferedImageLuminanceSource source = new BufferedImageLuminanceSource(image);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Result result;
        Hashtable hints = new Hashtable();
        hints.put(DecodeHintType.CHARACTER_SET, CHARSET);
        result = new MultiFormatReader().decode(bitmap, hints);
        String resultStr = result.getText();
        return resultStr;
    }


    public static String decode(String path) throws Exception {
        return QRCodeUtil.decode(new File(path));
    }


    public static void main(String[] args) {
        TestThread t1 = new TestThread();
        TestThred t2 = new TestThred();
        TestThred3 t3 = new TestThred3();
        Thread th = new Thread(t1);
        Thread th2 = new Thread(t2);
        Thread th3 = new Thread(t3);
        th.start();
        th2.start();
        th3.start();
/*        String text = "http://jc.cdhexinyi.cn/jc/testurlCode.html";
        try {
            for (int i = 0; i < 100000; i++) {
                QRCodeUtil.encode(text,*//*"D:\\MyWorkDoc\\qg.jpg",*//* "d:/MyWorkDoc", true);
                System.out.println("success");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }*/


    }

    //可以一个线程生成一种规格的二维码 128*128
    static class TestThread implements Runnable{
        @Override
        public void run() {
            try {
                for (int i = 0; i < 1000; i++) {
                    String text = "http://jc.cdhexinyi.cn/jc/testurlCode.html?args"+i;
                    String fileName = "MM"+"A"+"000"+"0x1234567";
                    QRCodeUtil.encode(text,/*"D:\\MyWorkDoc\\qg.jpg",*/ "d:/MyWorkDoc/"+fileName+i, true,0);
                    System.out.println(i+"one");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //256*256
    static class TestThred implements Runnable{
        @Override
        public void run() {
//            String text = "http://jc.cdhexinyi.cn/jc/testurlCode.html"+"";
            try {
                for (int i = 0; i < 1000; i++) {
                    String text = "http://jc.cdhexinyi.cn/jc/testurlCode.html?args"+i;
                    String fileName = "MM"+"A"+"000"+"0x1234567";
                    QRCodeUtil.encode(text,/*"D:\\MyWorkDoc\\qg.jpg",*/ "d:/MyWorkDoc/"+fileName+i, true,1);
                    System.out.println(i+"two");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //430*430
    static class TestThred3 implements Runnable{
        @Override
        public void run() {
//            String text = "http://jc.cdhexinyi.cn/jc/testurlCode.html"+"";
            try {
                for (int i = 0; i < 1000; i++) {
                    String text = "http://jc.cdhexinyi.cn/jc/testurlCode.html?args"+i;
                    String fileName = "MM"+"A"+"000"+"0x1234567";
                    QRCodeUtil.encode(text,/*"D:\\MyWorkDoc\\qg.jpg",*/ "d:/MyWorkDoc/"+fileName+i, true,2);
                    System.out.println(i+"three");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
