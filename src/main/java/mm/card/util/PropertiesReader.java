package mm.card.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 配置文件工具类
 *
 * @author Yif
 * @create 2016 10 20 18:08
 */
public class PropertiesReader {

    private static Logger logger = LoggerFactory.getLogger(PropertiesReader.class);

    /**
     * 读取配置文件中的属性
     * @param propertiesName 配置文件名称
     * @param key
     * @return
     */
    public static String getProperty(String propertiesName, String key) {
        Properties prop = new Properties();
        InputStream inputStream = PropertiesReader.class.getResourceAsStream(propertiesName);
        try {
            prop.load(inputStream);
            return prop.getProperty(key);
        } catch (Exception e) {
            logger.error("Read properties fail.", e);
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error("Closed InputStream error.", e);
                }
            }
        }
    }

    /**
     * 返回整个配置文件
     * @param propertiesName
     * @return
     */
    public static Properties getPropertyFile(String propertiesName) {
        Properties prop = new Properties();
        InputStream inputStream = PropertiesReader.class.getResourceAsStream(propertiesName);
        try {
            prop.load(inputStream);
            return prop;
        } catch (Exception e) {
            logger.error("Read properties fail.", e);
            return null;
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    logger.error("Closed InputStream error.", e);
                }
            }
        }
    }
}
