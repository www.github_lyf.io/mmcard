package mm.card.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalTimeUtil {
	
	public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

	public static final String TIMESTAMP_PATTERN = "yyyyMMddHHmmss";

	public static final String DATE_PATTERN = "yyyy-MM-dd";

	public static final String TIME_PATTERN = "HH:mm:ss";

	/**
	 * 返回当前日期。 yyyy-MM-dd
	 * 
	 * @return
	 */
	public static String getCurrentDateStr() {
		LocalDateTime currentTime = LocalDateTime.now();
		LocalDate date = currentTime.toLocalDate();
		return date.toString();
	}

	/**
	 * 返回当前日期。 yyyy-MM-dd HH:mm:ss
	 * 
	 * @return
	 */
	public static String getCurrentTimeStr() {
		LocalDateTime currentTime = LocalDateTime.now();
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATETIME_PATTERN);
		return currentTime.format(formatter);
	}
	
	/**
	 * 返回昨天日期。 yyyy-MM-dd
	 * 
	 * @return
	 */
	public static String getYesterday(){
		LocalDateTime currentTime = LocalDateTime.now();
		LocalDate date = currentTime.toLocalDate().plusDays(-1);
		return date.toString();
	}
	
	/**
	 * 获取距今天某个天数的时间  yyyy-MM-dd
	 * @param days 天数（有正负）
	 * @return
	 */
	public static String getDateSpaceStr(int days){
		LocalDateTime currentTime = LocalDateTime.now();
		LocalDate date = currentTime.toLocalDate().plusDays(days);
		return date.toString();
	}
	
	/**
	 * 获取当前年份。yyyy
	 * @return
	 */
	public static String getCurrentYearStr(){
		LocalDateTime currentTime = LocalDateTime.now();
		int year = currentTime.getYear();
		return year + "";
	}

	
}
