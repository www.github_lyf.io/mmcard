package mm.card.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DateUtil {
	
	public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";

	public static final String TIMESTAMP_PATTERN = "yyyyMMddHHmmss";

	public static final String DATE_PATTERN = "yyyy-MM-dd";

	public static final String TIME_PATTERN = "HH:mm:ss";

	public static final String YEAR_PATTERN = "yyyy";

	public static final String MONTH_PATTERN = "MM";
	
	public static final String DAY_PATTERN = "dd";

	/**
	 * 引入日志对象
	 */
	private static Logger logger = LoggerFactory.getLogger(DateUtil.class);
	
	/**
	 * 格式化样式一："yyyy-MM-dd HH:mm:ss"
	 * @param date 传入日期
	 * @return
	 */
	public static String dateTime2Str(Date date){
		if (date != null) {
			SimpleDateFormat formater = new SimpleDateFormat(DATETIME_PATTERN);
			return formater.format(date);
		}
		return null;
	}
	
	/**
	 * 格式化样式一："yyyy-MM-dd HH:mm:ss"
	 * @param str 传入日期字符串
	 * @return
	 */
	public static Date str2DateTime(String str){
		if (str != null) {
			SimpleDateFormat formater = new SimpleDateFormat(DATETIME_PATTERN);
			try {
				return formater.parse(str);
			} catch (ParseException e) {
				logger.error("DateUtil str2DateTime ParseException", e);
			}
		}
		return null;
	}
	
	/**
	 * 格式化样式二："yyyyMMddHHmmss"
	 * @param date 传入日期
	 * @return
	 */
	public static String timestamp2Str(Date date){
		if (date != null) {
			SimpleDateFormat formater = new SimpleDateFormat(TIMESTAMP_PATTERN);
			return formater.format(date);
		}
		return null;
	}
	
	/**
	 * 格式化样式二："yyyyMMddHHmmss"
	 * @param str 传入日期字符串
	 * @return
	 */
	public static Date str2Timestamp(String str){
		if (str != null) {
			SimpleDateFormat formater = new SimpleDateFormat(TIMESTAMP_PATTERN);
			try {
				return formater.parse(str);
			} catch (ParseException e) {
				logger.error("DateUtil str2Timestamp ParseException", e);
			}
		}
		return null;
	}
	
	/**
	 * 格式化样式三："yyyy-MM-dd"
	 * @param date 传入日期
	 * @return
	 */
	public static String date2Str(Date date){
		if (date != null) {
			SimpleDateFormat formater = new SimpleDateFormat(DATE_PATTERN);
			return formater.format(date);
		}
		return null;
	}
	
	/**
	 * 格式化样式三："yyyy-MM-dd"
	 * @param str 传入日期字符串
	 * @return
	 */
	public static Date str2Date(String str){
		if (str != null) {
			SimpleDateFormat formater = new SimpleDateFormat(DATE_PATTERN);
			try {
				return formater.parse(str);
			} catch (ParseException e) {
				logger.error("DateUtil str2Timestamp ParseException", e);
			}
		}
		return null;
	}

	/**
	 * 以"yyyy-MM-dd HH:mm:ss"格式化当前日期
	 *
	 * @return
	 */
	public static String getCurrentTimeStr() {

		SimpleDateFormat formater = new SimpleDateFormat(DATETIME_PATTERN);
		return formater.format(new Date());
	}

	/**
	 * 以"yyyy-MM-dd"格式化日期
	 * 
	 * @return
	 */
	public static String getDateStr(Date date) {
		SimpleDateFormat formater = new SimpleDateFormat(DATE_PATTERN);
		return formater.format(date);
	}
	
	/**
	 * 获取今年年份yyyy
	 * 
	 * @return
	 */
	public static String getYearStr() {
		SimpleDateFormat formater = new SimpleDateFormat(YEAR_PATTERN);
		return formater.format(new Date());
	}

	/**
	 * 获取今年月份MM
	 * 
	 * @return
	 */
	public static String getMonthStr() {
		SimpleDateFormat formater = new SimpleDateFormat(MONTH_PATTERN);
		return formater.format(new Date());
	}

	/**
	 * 获取今天
	 * 
	 * @return
	 */
	public static String getDayStr() {
		SimpleDateFormat formater = new SimpleDateFormat(DAY_PATTERN);
		return formater.format(new Date());
	}

	/**
	 * 获取昨天日期
	 * 
	 * @return
	 */
	public static Date getYesterdayDate() {
		Calendar ca = Calendar.getInstance();
		ca.setTime(new Date());
		ca.add(Calendar.DATE, -1);
		return ca.getTime();
	}

	/**
	 * 根据传入的日期和格式,格式化此日期
	 * 
	 * @param date
	 *            传入日期
	 * @param pattern
	 *            传入格式
	 * @return
	 */
	public static String dateFormatByPattern(Date date, String pattern) {
		if ((date == null) || (pattern == null)) {
			return null;
		}

		String dateStr = null;
		try {
			SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
			dateStr = dateFormat.format(date);
		} catch (Exception e) {
			logger.error("dateFormatBypattern is Exception", e);
		}
		return dateStr;
	}

}
