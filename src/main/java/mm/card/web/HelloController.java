package mm.card.web;

import mm.card.pojo.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Yif on 2016/9/27.
 */
@Controller
@RequestMapping("/user")
public class HelloController {
    private Logger logger = LoggerFactory.getLogger(HelloController.class);


    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String hello(Model model) {
        model.addAttribute("name", "Dear");
        return "hello";
    }


    @PreAuthorize("hasAuthority('ROLE_USER')")//这里可以指定特定角色的用户访问
    @RequestMapping(value = "/thy",produces = "application/json;charset=UTF-8",method = RequestMethod.GET)
    public String testThymeleaf(Model model){
        return "thy";
    }

    @RequestMapping("/login")
//    @ResponseBody
    public String login(HttpServletRequest request,HttpServletResponse response) throws IOException {
          //response.sendRedirect("/login2.html");
        //MyAuthenticationProvider provider = new MyAuthenticationProvider();
        System.out.println("test");

        return "login";
    }

    @RequestMapping("/success")
    @ResponseBody
    public Response success(HttpServletRequest request){
        Response user = new Response();
        List list = new ArrayList<>();
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication() .getPrincipal();
        System.out.println("密码是"+userDetails.getPassword());
        System.out.println("用户名是" + userDetails.getUsername());
        List<GrantedAuthority> authorities2 = (List<GrantedAuthority>) userDetails.getAuthorities();


        for (GrantedAuthority grantedAuthority : authorities2) {
            list.add(grantedAuthority.getAuthority());
            System.out.println("Authority" + grantedAuthority.getAuthority());

        }
       SecurityContextImpl securityContextImpl = (SecurityContextImpl) request
                .getSession().getAttribute("SPRING_SECURITY_CONTEXT");

        // 登录名
        System.out.println("Username:"
                + securityContextImpl.getAuthentication().getName());
        // 登录密码，未加密的
        System.out.println("Credentials:"
                + securityContextImpl.getAuthentication().getCredentials());
        WebAuthenticationDetails details = (WebAuthenticationDetails) securityContextImpl
                .getAuthentication().getDetails();
        // 获得访问地址
        System.out.println("RemoteAddress" + details.getRemoteAddress());
        // 获得sessionid
        System.out.println("SessionId" + details.getSessionId());
        // 获得当前用户所拥有的权限
/*        List<GrantedAuthority> authorities = (List<GrantedAuthority>) securityContextImpl
                .getAuthentication().getAuthorities();

        for (GrantedAuthority grantedAuthority : authorities) {
            list.add(grantedAuthority.getAuthority());
            System.out.println("Authority" + grantedAuthority.getAuthority());

        }*/
        user.setSessionId(details.getSessionId());
        user.setUserRole(list);
        user.setUserName(securityContextImpl.getAuthentication().getName());
        user.setPassword(userDetails.getPassword());

        return user;
    }


/*    @RequestMapping("/logout")
    public String logout(HttpServletRequest request){
        System.out.println("退出进入");
        return "login";
    }*/
}
