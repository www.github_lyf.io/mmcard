/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50713
Source Host           : localhost:3306
Source Database       : mm_card

Target Server Type    : MYSQL
Target Server Version : 50713
File Encoding         : 65001

Date: 2016-10-19 16:28:28
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for card_admin_info
-- ----------------------------
DROP TABLE IF EXISTS `card_admin_info`;
CREATE TABLE `card_admin_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `admin_id` varchar(255) NOT NULL COMMENT '管理员ID',
  `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `user_role` varchar(255) DEFAULT NULL COMMENT '角色',
  `pwd_error_count` int(11) NOT NULL DEFAULT '0' COMMENT '密码错误次数',
  `is_lock` int(11) NOT NULL DEFAULT '0' COMMENT '是否锁定',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `is_active` int(11) NOT NULL DEFAULT '0' COMMENT '是否激活',
  `is_valid` int(11) NOT NULL DEFAULT '1' COMMENT '是否有效',
  `is_delete` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_admin_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_agent_info
-- ----------------------------
DROP TABLE IF EXISTS `card_agent_info`;
CREATE TABLE `card_agent_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `agent_id` varchar(255) NOT NULL COMMENT '代理商ID',
  `rate` decimal(18,6) NOT NULL DEFAULT '0.000000' COMMENT '每笔交易被抽成比例',
  `user_role` varchar(255) NOT NULL COMMENT '用户角色',
  `parent_agent_id` varchar(255) DEFAULT NULL COMMENT '上级代理ID',
  `account_balance` decimal(18,6) NOT NULL DEFAULT '0.000000' COMMENT '账户余额',
  `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `name` varchar(255) DEFAULT NULL COMMENT '代理商名称',
  `linkman` varchar(255) DEFAULT NULL COMMENT '联系人',
  `linkman_phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `regist_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '注册时间',
  `last_login_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '最后登录时间',
  `pwd_err_count` int(11) NOT NULL DEFAULT '0' COMMENT '密码错误次数',
  `is_lock` int(11) NOT NULL DEFAULT '0' COMMENT '是否锁定',
  `is_active` int(11) NOT NULL DEFAULT '0' COMMENT '是否激活',
  `is_valid` int(11) NOT NULL DEFAULT '1' COMMENT '是否有效',
  `is_delete` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_agent_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_agent_qrcode_info
-- ----------------------------
DROP TABLE IF EXISTS `card_agent_qrcode_info`;
CREATE TABLE `card_agent_qrcode_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `agent_id` varchar(255) NOT NULL COMMENT '代理商ID',
  `qrcode_start` varchar(255) DEFAULT NULL COMMENT '二维码起始ID',
  `qrcode_end` varchar(255) DEFAULT NULL COMMENT '二维码结束ID',
  `qrcode_count` int(11) DEFAULT NULL COMMENT '二维码数量',
  `qrcode_active_count` int(11) DEFAULT NULL COMMENT '二维码激活数量',
  `provice` varchar(255) DEFAULT NULL COMMENT '省份',
  `city` varchar(255) DEFAULT NULL COMMENT '城市',
  `county` varchar(255) DEFAULT NULL COMMENT '县区',
  `is_valid` int(11) NOT NULL DEFAULT '1' COMMENT '是否有效',
  `is_delete` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_agent_qrcode_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_order_info
-- ----------------------------
DROP TABLE IF EXISTS `card_order_info`;
CREATE TABLE `card_order_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL,
  `order_money` decimal(18,6) DEFAULT NULL,
  `rate` decimal(18,6) DEFAULT NULL COMMENT '总抽成费率。shop_money=order_money*(1-rate)',
  `shop_id` varchar(255) DEFAULT NULL,
  `shop_name` varchar(255) DEFAULT NULL,
  `shop_money` decimal(18,6) DEFAULT NULL,
  `qrcode_id` varchar(255) DEFAULT NULL,
  `bank` varchar(255) DEFAULT NULL,
  `bank_code` varchar(255) DEFAULT NULL,
  `bank_card` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `pay_time` datetime DEFAULT NULL,
  `pay_status` int(11) DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_order_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_order_success_info
-- ----------------------------
DROP TABLE IF EXISTS `card_order_success_info`;
CREATE TABLE `card_order_success_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) NOT NULL COMMENT '订单ID',
  `order_money` decimal(18,6) DEFAULT NULL COMMENT '订单金额',
  `rate` decimal(18,6) DEFAULT NULL COMMENT '总抽成费率。shop_money=order_money*(1-rate)',
  `qrcode_id` varchar(255) DEFAULT NULL COMMENT '二维码ID',
  `shop_id` varchar(255) DEFAULT NULL COMMENT '商铺ID',
  `shop_name` varchar(255) DEFAULT NULL COMMENT '商铺名称',
  `shop_money` decimal(18,6) DEFAULT NULL COMMENT '商铺获得的钱',
  `bank` varchar(255) DEFAULT NULL COMMENT '商铺开户银行',
  `bank_code` varchar(255) DEFAULT NULL COMMENT '商铺开户银行代码',
  `bank_card` varchar(255) DEFAULT NULL COMMENT '商户银行卡号',
  `address` varchar(255) DEFAULT NULL COMMENT '商铺地址',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `pay_time` datetime DEFAULT NULL COMMENT '支付时间',
  `pay_status` int(11) DEFAULT NULL COMMENT '支付状态',
  `agent_a` varchar(255) DEFAULT NULL COMMENT '代理商A',
  `agent_a_money` varchar(255) DEFAULT NULL COMMENT '代理商A提成',
  `agent_b` varchar(255) DEFAULT NULL COMMENT '代理商B',
  `agent_b_money` varchar(255) DEFAULT NULL,
  `agent_c` varchar(255) DEFAULT NULL,
  `agent_c_money` varchar(255) DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_order_success_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_permission_info
-- ----------------------------
DROP TABLE IF EXISTS `card_permission_info`;
CREATE TABLE `card_permission_info` (
  `permission_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `relative_path` varchar(255) DEFAULT NULL COMMENT 'url的相对路径',
  `description` varchar(255) DEFAULT NULL COMMENT '说明',
  `depend_permission` int(11) DEFAULT NULL COMMENT '依赖的其他权限',
  `parent_permission` int(11) DEFAULT NULL COMMENT '父级权限',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_valid` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of card_permission_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_qrcode_info
-- ----------------------------
DROP TABLE IF EXISTS `card_qrcode_info`;
CREATE TABLE `card_qrcode_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `qrcode_id` varchar(255) NOT NULL COMMENT '二维码ID',
  `agent_a` varchar(255) NOT NULL COMMENT 'A级代理',
  `agent_a_regist_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '分发给代理商A的时间',
  `agent_b` varchar(255) DEFAULT NULL COMMENT 'B级代理',
  `agent_b_regist_time` datetime DEFAULT NULL COMMENT '分发给代理商B的时间',
  `agent_c` varchar(255) DEFAULT NULL COMMENT 'C级代理',
  `agent_c_regist_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '分发给代理商C的时间',
  `shop` varchar(255) DEFAULT NULL COMMENT '商铺ID',
  `shop_regist_time` datetime DEFAULT NULL COMMENT '商铺注册的时间',
  `provice` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `county` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '0' COMMENT '是否激活',
  `is_valid` int(11) NOT NULL DEFAULT '1' COMMENT '是否有效',
  `is_delete` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='二维码信息表';

-- ----------------------------
-- Records of card_qrcode_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_role_info
-- ----------------------------
DROP TABLE IF EXISTS `card_role_info`;
CREATE TABLE `card_role_info` (
  `role_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_valid` int(11) DEFAULT '1',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_role_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_role_permission_info
-- ----------------------------
DROP TABLE IF EXISTS `card_role_permission_info`;
CREATE TABLE `card_role_permission_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `is_valid` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_role_permission_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_shop_info
-- ----------------------------
DROP TABLE IF EXISTS `card_shop_info`;
CREATE TABLE `card_shop_info` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `shop_id` varchar(255) NOT NULL COMMENT '商铺ID',
  `qrcode_id` varchar(255) NOT NULL COMMENT '二维码ID',
  `user_name` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `name` varchar(255) DEFAULT NULL COMMENT '商铺名',
  `phone` varchar(255) DEFAULT NULL COMMENT '联系方式',
  `provice` varchar(255) DEFAULT NULL COMMENT '省份',
  `city` varchar(255) DEFAULT NULL COMMENT '城市',
  `county` varchar(255) DEFAULT NULL COMMENT '县区',
  `bank` varchar(255) DEFAULT NULL COMMENT '开户银行名称',
  `bank_code` varchar(255) DEFAULT NULL COMMENT '开户银行代码',
  `bank_card` varchar(255) DEFAULT NULL COMMENT '银行卡号',
  `regist_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '注册时间',
  `pwd_error_count` int(11) NOT NULL DEFAULT '0' COMMENT '密码错误次数',
  `is_lock` int(11) NOT NULL DEFAULT '0' COMMENT '是否锁定',
  `last_login_time` datetime DEFAULT NULL COMMENT '最后登录时间',
  `is_active` int(11) NOT NULL DEFAULT '0' COMMENT '是否激活',
  `is_valid` int(11) NOT NULL DEFAULT '1' COMMENT '是否有效',
  `is_delete` int(11) NOT NULL DEFAULT '0' COMMENT '是否删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_shop_info
-- ----------------------------

-- ----------------------------
-- Table structure for card_table_1
-- ----------------------------
DROP TABLE IF EXISTS `card_table_1`;
CREATE TABLE `card_table_1` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_1` varchar(255) NOT NULL,
  `attribute_2` varchar(255) DEFAULT NULL,
  `attribute_3` varchar(255) DEFAULT NULL,
  `attribute_4` varchar(255) DEFAULT NULL,
  `attribute_5` varchar(255) DEFAULT NULL,
  `attribute_6` varchar(255) DEFAULT NULL,
  `attribute_7` varchar(255) DEFAULT NULL,
  `attribute_8` varchar(255) DEFAULT NULL,
  `attribute_9` varchar(255) DEFAULT NULL,
  `attribute_10` varchar(255) DEFAULT NULL,
  `attribute_11` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_table_1
-- ----------------------------

-- ----------------------------
-- Table structure for card_table_2
-- ----------------------------
DROP TABLE IF EXISTS `card_table_2`;
CREATE TABLE `card_table_2` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_1` varchar(255) NOT NULL,
  `attribute_2` varchar(255) DEFAULT NULL,
  `attribute_3` varchar(255) DEFAULT NULL,
  `attribute_4` varchar(255) DEFAULT NULL,
  `attribute_5` varchar(255) DEFAULT NULL,
  `attribute_6` varchar(255) DEFAULT NULL,
  `attribute_7` varchar(255) DEFAULT NULL,
  `attribute_8` varchar(255) DEFAULT NULL,
  `attribute_9` varchar(255) DEFAULT NULL,
  `attribute_10` varchar(255) DEFAULT NULL,
  `attribute_11` varchar(255) DEFAULT NULL,
  `attribute_12` varchar(255) DEFAULT NULL,
  `attribute_13` varchar(255) DEFAULT NULL,
  `attribute_14` varchar(255) DEFAULT NULL,
  `attribute_15` varchar(255) DEFAULT NULL,
  `attribute_16` varchar(255) DEFAULT NULL,
  `attribute_17` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_table_2
-- ----------------------------

-- ----------------------------
-- Table structure for card_table_3
-- ----------------------------
DROP TABLE IF EXISTS `card_table_3`;
CREATE TABLE `card_table_3` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_1` varchar(255) NOT NULL,
  `attribute_2` varchar(255) DEFAULT NULL,
  `attribute_3` varchar(255) DEFAULT NULL,
  `attribute_4` varchar(255) DEFAULT NULL,
  `attribute_5` varchar(255) DEFAULT NULL,
  `attribute_6` varchar(255) DEFAULT NULL,
  `attribute_7` varchar(255) DEFAULT NULL,
  `attribute_8` varchar(255) DEFAULT NULL,
  `attribute_9` varchar(255) DEFAULT NULL,
  `attribute_10` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_table_3
-- ----------------------------

-- ----------------------------
-- Table structure for card_table_4
-- ----------------------------
DROP TABLE IF EXISTS `card_table_4`;
CREATE TABLE `card_table_4` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_1` varchar(255) NOT NULL,
  `attribute_2` varchar(255) DEFAULT NULL,
  `attribute_3` varchar(255) DEFAULT NULL,
  `attribute_4` varchar(255) DEFAULT NULL,
  `attribute_5` varchar(255) DEFAULT NULL,
  `attribute_6` varchar(255) DEFAULT NULL,
  `attribute_7` varchar(255) DEFAULT NULL,
  `attribute_8` varchar(255) DEFAULT NULL,
  `attribute_9` varchar(255) DEFAULT NULL,
  `attribute_10` varchar(255) DEFAULT NULL,
  `attribute_11` varchar(255) DEFAULT NULL,
  `attribute_12` varchar(255) DEFAULT NULL,
  `attribute_13` varchar(255) DEFAULT NULL,
  `attribute_14` varchar(255) DEFAULT NULL,
  `attribute_15` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_table_4
-- ----------------------------

-- ----------------------------
-- Table structure for card_table_5
-- ----------------------------
DROP TABLE IF EXISTS `card_table_5`;
CREATE TABLE `card_table_5` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_1` varchar(255) NOT NULL,
  `attribute_2` varchar(255) DEFAULT NULL,
  `attribute_3` varchar(255) DEFAULT NULL,
  `attribute_4` varchar(255) DEFAULT NULL,
  `attribute_5` varchar(255) DEFAULT NULL,
  `attribute_6` varchar(255) DEFAULT NULL,
  `attribute_7` varchar(255) DEFAULT NULL,
  `attribute_8` varchar(255) DEFAULT NULL,
  `attribute_9` varchar(255) DEFAULT NULL,
  `attribute_10` varchar(255) DEFAULT NULL,
  `attribute_11` varchar(255) DEFAULT NULL,
  `attribute_12` varchar(255) DEFAULT NULL,
  `attribute_13` varchar(255) DEFAULT NULL,
  `attribute_14` varchar(255) DEFAULT NULL,
  `attribute_15` varchar(255) DEFAULT NULL,
  `attribute_16` varchar(255) DEFAULT NULL,
  `attribute_17` varchar(255) DEFAULT NULL,
  `attribute_18` varchar(255) DEFAULT NULL,
  `attribute_19` varchar(255) DEFAULT NULL,
  `attribute_20` varchar(255) DEFAULT NULL,
  `attribute_21` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_table_5
-- ----------------------------

-- ----------------------------
-- Table structure for card_table_6
-- ----------------------------
DROP TABLE IF EXISTS `card_table_6`;
CREATE TABLE `card_table_6` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_1` varchar(255) NOT NULL,
  `attribute_2` varchar(255) DEFAULT NULL,
  `attribute_3` varchar(255) DEFAULT NULL,
  `attribute_4` varchar(255) DEFAULT NULL,
  `attribute_5` varchar(255) DEFAULT NULL,
  `attribute_6` varchar(255) DEFAULT NULL,
  `attribute_7` varchar(255) DEFAULT NULL,
  `attribute_8` varchar(255) DEFAULT NULL,
  `attribute_9` varchar(255) DEFAULT NULL,
  `attribute_10` varchar(255) DEFAULT NULL,
  `attribute_11` varchar(255) DEFAULT NULL,
  `attribute_12` varchar(255) DEFAULT NULL,
  `attribute_13` varchar(255) DEFAULT NULL,
  `attribute_14` varchar(255) DEFAULT NULL,
  `attribute_15` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_table_6
-- ----------------------------

-- ----------------------------
-- Table structure for card_table_7
-- ----------------------------
DROP TABLE IF EXISTS `card_table_7`;
CREATE TABLE `card_table_7` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attribute_1` varchar(255) NOT NULL,
  `attribute_2` varchar(255) DEFAULT NULL,
  `attribute_3` varchar(255) DEFAULT NULL,
  `attribute_4` varchar(255) DEFAULT NULL,
  `attribute_5` varchar(255) DEFAULT NULL,
  `attribute_6` varchar(255) DEFAULT NULL,
  `attribute_7` varchar(255) DEFAULT NULL,
  `attribute_8` varchar(255) DEFAULT NULL,
  `attribute_9` varchar(255) DEFAULT NULL,
  `attribute_10` varchar(255) DEFAULT NULL,
  `attribute_11` varchar(255) DEFAULT NULL,
  `attribute_12` varchar(255) DEFAULT NULL,
  `attribute_13` varchar(255) DEFAULT NULL,
  `attribute_14` varchar(255) DEFAULT NULL,
  `attribute_15` varchar(255) DEFAULT NULL,
  `attribute_16` varchar(255) DEFAULT NULL,
  `attribute_17` varchar(255) DEFAULT NULL,
  `attribute_18` varchar(255) DEFAULT NULL,
  `attribute_19` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of card_table_7
-- ----------------------------
SET FOREIGN_KEY_CHECKS=1;
